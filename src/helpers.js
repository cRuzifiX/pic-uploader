export const userAuthenticated = allowedDomains => {
  const userEmail = localStorage.getItem("user-email");
  if (userEmail) {
    const domain = userEmail.substr(userEmail.indexOf("@") + 1);
    if (allowedDomains.includes(domain)) {
      return true;
    }
    alert("Your are not authorized to upload images");
    return false;
  }
  alert("Please sign in to upload your images");
  return false;
};
