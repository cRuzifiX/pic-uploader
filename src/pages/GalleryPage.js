import React, { Component } from "react";
import Container from "../components/Container";

export default class HomePage extends Component {
  copyToClipboard = e => {
    const el = document.createElement("textarea");
    el.value = e.currentTarget.dataset.link;
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
  };

  render() {
    const result = this.props.location.state
      ? this.props.location.state.result
      : [];
    return (
      <Container>
        <div className="gallery">
          {!result.length && <span>Upload some cool images to view here...</span>}
          {result.map(imageResponse => {
            return (
              <div className="gallery__img-container">
                <div>
                  <button
                    className="gallery__copy-link-btn"
                    onClick={this.copyToClipboard}
                    data-link={imageResponse.data.s3_link}
                  >
                    Copy Link
                  </button>
                </div>
                <img
                  className="gallery__image"
                  src={imageResponse.data.s3_link}
                  alt="uploaded"
                />
                <div>
                  {/* <input
                    className="gallery__input"
                    type="text"
                    placeholder="Add a description"
                  /> */}
                </div>
              </div>
            );
          })}
        </div>
      </Container>
    );
  }
}
