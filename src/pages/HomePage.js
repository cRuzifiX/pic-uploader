import React, { Component } from "react";
import Container from "../components/Container";
import Dropzone from "../components/Dropzone";
import Urlupload from "../components/Urlupload";
import Loader from "../components/Loader";

export default class HomePage extends Component {
  state = {
    uploading: false
  };

  setUploading = value => this.setState({ uploading: value });

  render() {
    const { uploading } = this.state;
    return (
      <Container>
        <div className="upload-box">
          {uploading && <Loader />}
          <Dropzone
            history={this.props.history}
            setUploading={this.setUploading}
          />
          <Urlupload
            history={this.props.history}
            setUploading={this.setUploading}
          />
        </div>
      </Container>
    );
  }
}
