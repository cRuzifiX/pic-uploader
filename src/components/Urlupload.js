import React, { Component } from "react";
import _ from 'lodash';

export default class Urlupload extends Component {
  constructor(props) {
    super(props);
    this.testUrl = _.debounce(this.testUrl, 500)
  }
  state = {
    url: ""
  };

  // is_url = str => {
  //   const regexp = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
  //   if (regexp.test(str)) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // };

  handleChange = async e => {
    const url = e.target.value;
    this.setState({ url });
    this.testUrl(url);
    
    // console.log('urlTestResult', urlTestResult.headers.get('Content-Type'));
    // if (this.is_url(url)) {
    //   this.props.setUploading(true);
    //   const urlencoded = new URLSearchParams();
    //   urlencoded.append("url", url);
    //   const rawResult = await fetch(
    //     "https://db.tools.streamoid.com/feed/insecure/play_images/upload_s3/post/url/",
    //     {
    //       method: "POST",
    //       headers: {
    //         "Content-type": "application/x-www-form-urlencoded"
    //       },
    //       body: urlencoded
    //     }
    //   ).then(response => response.text());
    //   const result = JSON.parse(rawResult);
    //   if (result.status.message === "success") {
    //     this.props.setUploading(false);
    //     this.props.history.push("/gallery", { result: [result] });
    //   }
    // }
  };

  testUrl = async (url) => {
    const urlTestResult = await fetch(url);
    const contentType = urlTestResult.headers.get("Content-Type");
    if (url.length && contentType.includes("image")) {
      this.props.setUploading(true);
      const urlencoded = new URLSearchParams();
      urlencoded.append("url", url);
      const rawResult = await fetch(
        "https://db.tools.streamoid.com/feed/insecure/play_images/upload_s3/post/url/",
        {
          method: "POST",
          headers: {
            "Content-type": "application/x-www-form-urlencoded"
          },
          body: urlencoded
        }
      ).then(response => response.text());
      const result = JSON.parse(rawResult);
      if (result.status.message === "success") {
        this.props.setUploading(false);
        this.props.history.push("/gallery", { result: [result] });
      }
    }
  }

  render() {
    const { url } = this.state;
    return (
      <div className="urlupload">
        <input
          onChange={this.handleChange}
          placeholder="Paste url here"
          className="urlupload__input"
          value={url}
        />
      </div>
    );
  }
}
