import React, { useCallback } from "react";
import { useDropzone } from "react-dropzone";
import _ from "lodash";
import { userAuthenticated } from "../helpers";

const allowedDomains = ["streamoid.com", "gmail.com"];

function Dropzone(props) {
  const onDrop = useCallback(async acceptedFiles => {
    if (!userAuthenticated(allowedDomains)) {
      return;
    }
    props.setUploading(true);
    const result = await uploadImages(acceptedFiles);
    if (result.length) {
      props.setUploading(false);
      props.history.push("/gallery", { result });
    } else {
      props.setUploading(false);
      alert("Sorry. You cannot upload that type of file");
    }
  }, []);

  const uploadImages = async fileInput => {
    const promiseArr = fileInput.map((file, index) => {
      if (file.type.includes("image")) {
        const formdata = new FormData();
        formdata.append("image_data", file, file.name);

        var requestOptions = {
          method: "POST",
          body: formdata,
          redirect: "follow"
        };

        return fetch(
          "https://db.tools.streamoid.com/feed/insecure/play_images/upload_s3/post/image_data/",
          requestOptions
        ).then(response => response.text());
      }
      return null;
    });

    const filteredPromiseArr = _.compact(promiseArr);

    const results = await Promise.all(filteredPromiseArr);
    return parseResult(results);
  };

  const parseResult = results => results.map(result => JSON.parse(result));

  const { getRootProps, getInputProps } = useDropzone({ onDrop });

  return (
    <div className="dropzone" {...getRootProps()}>
      <div className="dropzone__box">
        <input {...getInputProps()} />
        <p>Drop images here</p>
      </div>
    </div>
  );
}

export default Dropzone;
