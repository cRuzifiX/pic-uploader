import React from "react";

const Loader = () => {
  const height = window.innerHeight;
  return (
    <div className="loader">
      <span>Uploading...</span>
    </div>
  );
};

export default Loader;
