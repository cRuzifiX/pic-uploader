import React, { Component } from "react";

export default class Container extends Component {
  render() {
    return (
      <div className="container">
        <div className="g-signin2 login-button" data-onsuccess="onSignIn" />
        {this.props.children}
      </div>
    );
  }
}
